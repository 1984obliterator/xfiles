6TH YEAR (A)

1. READING COMPREHENSION () <br>
GAME ON!

Are computer games bad for kids? Absolutely not! People who believe this obviously didn't grow up playing computer games. If you look at a first person shoot'em up game in a store, it looks like a solitary game. But according to research, about 60% of gamers play with friends. They take turns playing and giving advise so it's a social activity, not one that isolates people. In any case most computer games these days have at least some multiplayer functionality. And in case you hadn't realized, part of the success of the Wii console is that it is fun for the whole family, old and young.

There's another stereotype that I'd like to challenge. Computer games are not just a male activity. In fact, there's evidence that more girls than boys play computer games online. This should be no surprise as games designers have always been ahead of the movie business in providing positive role models for girls. Female game characters are often portrayed as powerful and independent. Look at the classic platform Tomb Raider. It's famous not only for its impressive graphics, but also for its fabulous main character. Lara Croft is a great role model for girls: she's tough and intelligent and leads an exciting international life.

It's important to note that computer games are not just for fun. They also teach a great deal. Most systems need to have simple gameplay to help people start quickly. But afterwards, users need to gain the confidence and determination to go up a level. This teaches us that in order to accomplish more in life we need to up our game! In addition, computer games have a strong narrative, so they teach people about storytelling, character and other things that we also learn from literature.

Finally, I'd like to look at the issue of computers and childhood obesity. Many children don't do a lot of exercise these days, but it's wrong to blame computer games for this.

Have you ever seen young kids jumping around with the controllers on their Wii system? With the new motion sensors on the next generation of consoles, the machine can even read the players' movements without a control. With a split-screen game, two children can play and do exercise at the same time. So games aren't responsible for poor health. The biggest problem is that adults don't let their children go out and play in case something bad happens to them. You can't blame computers for that.


1. READING COMPREHENSION () <br>
CAN COMPANIES MAKE PEOPLE MORE CREATIVE?

A lot of companies want their employees to be innovative and creative, but how can inventiveness be encauraged? We've all seen those pictures of striking, original or even truly weird offices, with ping-pong tables, swings, slides, even beds. But do they actually lead to greater creativity? It's hard to say. Having fun with colleagues on a slide might make you feel more relaxed and full of imaginative ideas, but then again, it might just be a silly waste of time.

Companies such as Google think that if they can get you bump into your colleagues all the time, you'll do more sharing of ideas. They have their employees eat lunch at long tables so that they will meet and chat to more people. All this chatting might spark some ideas, but on the other hand, maybe a nice quiet office and some peace to think might actually be more user-friendly.

Another approach that some companies take is to try to make sure that you have nothing else to worry about other than coming up with new impressive ideas. At Google, for example, breakfast, lunch and dinner are all provided free and you can get and you can get your hair cut, get your bike repaired, have your car washed or serviced, or even get your washing done, without having to leave the building. Again, these could be seen as benefits, but they could also just be a way of getting you to stay longer at work.

Ultimately, while companies like this could be fun places to work at, people actually often come up with the best ideas in spite of their environment, rather than because of it. J.K. Rowling famously wrote Harry Potter books while living on virtually no money, with a child to support.

As Steve Jobs, the founder of Apple, once said about the difficult years of his company: "We had nothing to lose and everything to gain." Perhaps to be truly creative you need to be a bit less comfortable so you're "hungry" to succeed?


